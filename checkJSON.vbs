' by Marcin Murawski | created: 2016-04-18 | last update: 2016-04-18
'
' WHAT
' Script searches all JSON files from current folder and lists their elements with their count and example content.
'
' HOW-TO
' Just run it in folder and wait until 'Done' message box appears.
'
' RESULTS
' Tab-delimited file called 'checkJSON.log' will be created. It can be easily imported into Excel sheet.
'

Dim fso, JSON, currFolder, regEx, line, id, content, elements(1024, 3), elID, loger, searchID

Set fso = CreateObject("Scripting.FileSystemObject")
Set regEx = new RegExp
regEx.Pattern = "(\w+?)"".*?""(.*?)"""

currFolder = fso.GetAbsolutePathName(".") & "\"

Set loger = fso.CreateTextFile(currFolder & "checkJSON.log")

elID = 0
elements(elID, 0) = "Element name"
elements(elID, 1) = "Content (example)"
elements(elID, 2) = "Count"

For Each file In fso.GetFolder(currFolder).Files
	If fso.GetExtensionName(file) = "json" Then
		Set JSON = fso.OpenTextFile(file.Path, 1)
		
		Do While JSON.AtEndOfStream <> True
			line = JSON.ReadLine
			
			If regEx.Test(line) Then
				id = regEx.Execute(line)(0).SubMatches(0)
				content = regEx.Execute(line)(0).SubMatches(1)
				searchID = searchArray(elements, id)
				If searchID = -1 Then
					elID = elID + 1
					elements(elID, 0) = id
					elements(elID, 1) = content
					elements(elID, 2) = 1
					
				ElseIf elements(searchID, 1) = "" And content <> "" Then
					elements(searchID, 1) = content
					elements(searchID, 2) = elements(searchID, 2) + 1
				Else
					elements(searchID, 2) = elements(searchID, 2) + 1
				End If
				
			End If
			
		Loop
		
	End If

Next

For i = 0 To 1023
	If elements(i, 0) <> "" Then
		loger.WriteLine elements(i, 2) & vbTab &  elements(i, 0) & vbTab & elements(i, 1)
	End If
Next

MsgBox "Done!"


Function searchArray(ByRef arr, ByRef tekst)

	searchArray = -1
	Dim i
	For i = 0 To 1023
		If arr(i, 0) = tekst Then
			searchArray = i
		End If
	Next

End Function





