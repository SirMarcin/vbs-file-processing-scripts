Dim fso, currFolder, loger, logFile, xlsx, fileName, fileExtension, filePath

Set fso = CreateObject("Scripting.FileSystemObject")
Set xlsx = CreateObject("Excel.Application")
xlsx.DisplayAlerts = false

currFolder = fso.GetAbsolutePathName(".") & "\"

For Each currFile in fso.GetFolder(currFolder).Files
    fileName = currFile.Name
    filePath = currFile.Path
    fileExtension = fso.GetExtensionName(currFile)
    
    If (fileExtension = "xlsx" Or fileExtension = "xls" Or fileExtension = "xlsm") Then
        'On Error Resume Next
        xlsx.Workbooks.Open(filePath)
        WScript.Echo fileName
        
        xlsx.ActiveSheet.SaveAs filePath & ".txt", 42
        
        xlsx.Workbooks.Close
        
    End If
    
Next