Dim folder, currentFile, loger
Dim fso, xls, w, i

Set fso = CreateObject("Scripting.FileSystemObject")
Set xls = CreateObject("Excel.Application")
xls.DisplayAlerts = False


folder = fso.GetAbsolutePathName(".") & "\"
Set loger = fso.CreateTextFile(folder & "ExcelSheetChecker.log", True)

For Each currentFile In fso.GetFolder(folder).Files
	If fso.GetExtensionName(currentFile.Name) <> "xlsx" Then
	Else

	i = 1

	'MsgBox currentFile.Name
	
	On Error Resume Next
	
	xls.Workbooks.Open(currentFile)
	
	loger.WriteLine currentFile.Name
	WScript.Echo currentFile.Name
	
	For Each w in xls.Worksheets
		Wscript.Echo " - " & w.Name
		loger.WriteLine " - " & w.Name
	Next
	
	xls.ActiveWorkbook.Save
	xls.Workbooks.Close
	
	loger.WriteLine ""
	WScript.Echo ""
	
	End If
Next