' Marcin Murawski || Created: 2016-05-06 || Last modified: 2016-05-06
'
' Script merges all UTF8 (!) *.txt files into one output file
'

Dim fso, currFolder, merged, mergedFile, tgt, fileName, fileExtension, filePath, tgtText

Set fso = CreateObject("Scripting.FileSystemObject")

currFolder = fso.GetAbsolutePathName(".") & "\"

mergedFile = currFolder & "merged.txt"

Set merged = CreateObject("ADODB.Stream")
merged.Type = 2
merged.Charset = "utf-8"
merged.Open
Set tgt = CreateObject("ADODB.Stream")
tgt.Charset = "utf-8"


For Each currFile in fso.GetFolder(currFolder).Files
    fileName = currFile.Name
    filePath = currFile.Path
    fileExtension = fso.GetExtensionName(currFile)
    
    If (fileExtension = "txt") Then
        
        WScript.Echo fileName
        
        tgt.Open
        tgt.LoadFromFile(filePath)
        tgtText = tgt.ReadText()
        tgt.Close
        
        merged.WriteText tgtText
    
    End If

Next

merged.SaveToFile mergedFile, 2
merged.Close
