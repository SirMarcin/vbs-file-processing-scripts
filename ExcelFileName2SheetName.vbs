' by Marcin Murawski || 2016-03-14
'
' script renames first sheet name from all Excel files from the directory it runs
' into first 31 characters of file name
'
' HOW TO RUN:
' Just run the script in the folder with Excel files.



Dim folder, currentFile, loger, newSheetName
Dim fso, xls, w, i, fileCount

Set fso = CreateObject("Scripting.FileSystemObject")
Set xls = CreateObject("Excel.Application")
xls.DisplayAlerts = False
fileCount = 0

folder = fso.GetAbsolutePathName(".") & "\"
Set loger = fso.CreateTextFile(folder & "ExcelFileName2SheetName.log", True)

' loop through folder looking for XLS or XLSX
For Each currentFile In fso.GetFolder(folder).Files
	If fso.GetExtensionName(currentFile.Name) = "xlsx" Or fso.GetExtensionName(currentFile.Name) = "xls" Then
	
		newSheetName = Left(currentFile.Name, 31)
		
		i = 1
		
		On Error Resume Next
		xls.Workbooks.Open(currentFile)
			
		If Err <> 0 Then
			loger.WriteLine "problem opening " & currentFile.Name
			
		Else
			
			On Error Resume Next
			
			xls.Sheets(1).Name = newSheetName
			fileCount = fileCount + 1
			
			xls.ActiveWorkbook.Save
			xls.Workbooks.Close
		End If
	End If
Next

MsgBox "Done! Check the log file for errors, if any." & vbNewLine & vbNewLine & "Number of processed files: " & fileCount