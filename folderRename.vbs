' by Marcin Murawski || created: 2016-08-30 || last modified: 2016-08-30
'
' script renames folders that come from TMS into folders that can be copied directly into Studio project structure
'

Dim currFolder, folder, fso

Set fso = CreateObject("Scripting.FileSystemObject")

currFolder = fso.GetAbsolutePathName(".") & "\"

For Each folder In fso.GetFolder(currFolder).SubFolders
	'WScript.Echo folder.Name
	RenameFolder folder.Name
Next

Function RenameFolder(ByVal name)
	Select Case name
		Case "EN-US_ES" fso.MoveFolder name, "es-ES"
		Case "EN-US_FR" fso.MoveFolder name, "fr-FR"
		Case "EN-US_JA" fso.MoveFolder name, "ja-JP"
		Case "EN-US_KO" fso.MoveFolder name, "ko-KR"
		Case "EN-US_PT-BR" fso.MoveFolder name, "pt-BR"
		Case "EN-US_RU" fso.MoveFolder name, "ru-RU"
		Case "EN-US_ZH-CN" fso.MoveFolder name, "zh-CN"
	End Select
		
End Function