' by Marcin Murawski || 2015-09-03
'
' script renames all sheet names from all Excel files from the directory it runs
' into default 'Sheet*' (or other, if specified), adding correct numbering
'
' HOW TO RUN:
' Just run the script in the folder with Excel files.
' You can specify the new sheet name by adding it as parameter, like here:
'
' 	ExcelSheetRenamer.vbs NewSheetName
'
' If you won't specify new sheet name, it'll use the default, 'Sheet' name
'


Dim folder, currentFile, loger, newSheetName
Dim fso, xls, w, i

Set fso = CreateObject("Scripting.FileSystemObject")
Set xls = CreateObject("Excel.Application")
xls.DisplayAlerts = False

' if script runs with paramater, use it for new sheet name
If Wscript.Arguments.Length = 1 Then
	newSheetName = Wscript.Arguments(0)
Else
	newSheetName = "Sheet"
End If

Wscript.Echo "new sheet name: " & newSheetName & vbCrLF

folder = fso.GetAbsolutePathName(".") & "\"
Set loger = fso.CreateTextFile(folder & "ExcelSheetRenamer.log", True)

' loop through folder looking for XLS or XLSX
For Each currentFile In fso.GetFolder(folder).Files
	If fso.GetExtensionName(currentFile.Name) = "xlsx" Or fso.GetExtensionName(currentFile.Name) = "xls" Then
	
		Wscript.Echo vbCrLF & currentFile.Name
		
		i = 1
		
		On Error Resume Next
		xls.Workbooks.Open(currentFile)
			
		If Err <> 0 Then
			loger.WriteLine " === problem opening " & currentFile.Name & " === "
			
		Else
			
			On Error Resume Next
			
			For Each w in xls.Worksheets
				If w.Name <> newSheetName & i Then
					Wscript.Echo "changing '" & w.Name & "' into '" & newSheetName & i & "'"
					loger.WriteLine currentFile.Name & " | changed '" & w.Name & "' into '" & newSheetName & i & "'"
				Else
					Wscript.Echo "sheet name '" & w.Name & "' is fine, no change"
					loger.WriteLine currentFile.Name & " | sheet name '" & w.Name & "' is fine, no change"
				
				End If
				
				w.Name = newSheetName & i
				i = i + 1
			Next
			
			xls.ActiveWorkbook.Save
			xls.Workbooks.Close
		End If
	End If
Next