' Marcin Murawski || created: 2016-03-23 || last update: 2016-04-01
'
' Script checks version of xml-based office files.
'
' = How-to =
' Just run it in folder with Office files; all subfolders will be checked
'
' = Results =
' A log file named 'file_list.log' will be created. It'll contain a list of all Office files and their version.
'
'
'
' CHANGELOG
'
' 0.8 - initial
' 1.0 - implemented custom, standalone ZIP module
'	  - added support for namespaces in XML
' 1.1 - added error handling
' 1.2 - added support for subfolders - now script searches all subfolders, including root folder
' 	  - fixed a bug where temporary files were deleted only after script finished, instead of immediately after they're not needed
'
'
'


dim currFolder, tempFolder, currFile, shlObj, fso, appXml, appXmlPath, loger, logerTXT, value, extension, appName, version, currentFileName, shlApp, fileCount
dim fileList, folder, folders

Set fso = CreateObject("Scripting.FileSystemObject")
Set shlObj = CreateObject("wscript.shell")				' for running CMD commands
Set shlApp = CreateObject("Shell.Application")			' for unzip purpouse
Set fileList = CreateObject("Scripting.Dictionary")		' map of all folders paths

currFolder = fso.GetAbsolutePathName(".") & "\"

' gather all subfolders paths
Set fileList = getSubfoldersList (currFolder, true)
folders = fileList.Items

fileCount = 0

' request a temporary folder from system
Set tempFolder = CreateTempFolder

On Error Resume Next

' set output for logging purpouse
Set loger = fso.CreateTextFile(currFolder & "___officeVersion___.csv", True)
Set logerTXT = fso.CreateTextFile(currFolder & "___officeVersion___.log", True)

' proceed only if log files could be created
If Err <> 0 Then
	Err.Clear
	MsgBox "Couldn't create log files - make sure any previous log files are closed or that you have write rights in current folder."
Else

	loger.WriteLine """" & "File name" & """,""" & "Version" & """"
	logerTXT.WriteLine "File name" & vbTab & "Version"

	' loop through all subfolders
	For Each folder In folders
	
		' loop through all files within current subfolder
		For Each currentFile In fso.GetFolder(folder).Files
			extension = fso.GetExtensionName(currentFile.Name)
			
			If extension = "xlsx" Or extension = "docx" Or extension = "pptx"  Or extension = "pptm"  Or extension = "docm"  Or extension = "xlsm" Then
				
				currentFileName = tempFolder.Path & "\" & currentFile.Name
				
				' copy current file to temporary folder (as .zip file - that's the only way to use system default unzipper)
				fso.CopyFile currentFile.Path, currentFileName & ".zip"
				fso.CreateFolder currentFileName & "temp\"
				
				' handle the extraction of current file 
				Set zipItems = shlApp.NameSpace(currentFileName & ".zip").items
				shlApp.NameSpace(currentFileName & "temp\").CopyHere(zipItems)
				
				
				Select Case extension
					Case "xlsx"	appName = "Excel"
					Case "xlsm"	appName = "Excel"
					Case "docx"	appName = "Word"
					Case "docm"	appName = "Word"
					Case "pptx"	appName = "PowerPoint"
					Case "pptm"	appName = "PowerPoint"
				End Select
				
				Set appXml = CreateObject("MSXML.DOMDocument")
				appXml.setProperty "SelectionLanguage", "XPath"
				
				appXmlPath = currentFileName & "temp\docProps\app.xml"
				appXml.Load (appXmlPath)
			
				On Error Resume Next
				
				' read the attribute that contains file version
				value = appXml.selectSingleNode("//*[local-name() = 'AppVersion']").text
					
				If Err = 0 Then
				
					Select Case Left(value, 2)
						Case "12" version = "2007"
						Case "14" version = "2010"
						Case "15" version = "2013"
						Case "16" version = "2016"
						Case Else version = value
					End Select
					
					' write all gathered information to log files
					loger.WriteLine """" & currentFile.Path & """,""" & appName & " " & version & """"
					logerTXT.WriteLine currentFile.Path & vbTab & appName & " " & version
					
					Set appXml = Nothing
					
				Else
					Err.Clear
					
					loger.WriteLine """" & currentFile.Path & """,""" & "There's a problem with structure of this file" & """"
					logerTXT.WriteLine currentFile.Path & vbTab & "There's a problem with structure of this file"
				End If

				fileCount = fileCount + 1
				
				' delete current temporary files
				fso.DeleteFolder currentFileName & "temp", true
				fso.DeleteFile currentFileName & ".zip", true
			
			End If
			
		Next

	Next
	
	' delete main temporary folder and clean all objects
	fso.DeleteFolder tempFolder, true
	Set loger = Nothing
	Set logerTXT = Nothing

	On Error Resume Next
	shlObj.Run currFolder & "___officeVersion___.csv"

	MsgBox "Done!" & vbNewLine & vbNewLine & "Number of files processed: " & fileCount

End If
	
' ====== FUNCTIONS ====== '

Function CreateTempFolder 
   Dim tfolder, tname, tpath
   Const TemporaryFolder = 2
   Set tfolder = fso.GetSpecialFolder(TemporaryFolder)
   tname = fso.GetTempName
   tname = Left(tname, Len(tname) - 4) & "DUPA"
   tpath = fso.GetAbsolutePathName(tfolder) & "\"
   Set tempFolder = fso.CreateFolder(tpath & tname)
   Set CreateTempFolder = tempFolder
End Function

' ===== FUNKCJE DO ZWRACANIA LISTY PODFOLDEROW =====

Function getSubfoldersList(currentFolderPath, rootFolder)
	Set fsoFL = CreateObject("Scripting.FileSystemObject")
	Set folderList = CreateObject("Scripting.Dictionary")
	Dim folderListID

	folderListID = 0

	If rootFolder = true Then
		folderList.Add folderListID, currentFolderPath
		folderListID = folderListID + 1
	End If

	ShowSubfolders fsoFL.GetFolder(currentFolderPath), folderList, folderListID

	Set getSubfoldersList = folderList
End Function

Function ShowSubFolders(Folder, folderList, folderListID)
	For Each Subfolder in Folder.SubFolders
		folderList.Add folderListID, Subfolder.Path
		folderListID = folderListID + 1
		ShowSubFolders Subfolder, folderList, folderListID
	Next
End Function

' ===== FUNKCJE DO ZWRACANIA LISTY PODFOLDEROW =====













