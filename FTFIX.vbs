' by Marcin Murawski || created: 2016-04-08 || last update: 2016-05-05
'
'
' 0.5:		 - initial
' 0.5.8:	 - added additional file types to auto-recognition method
'			 - improved 'guessing' method to check deeper into the file
' 0.6:		 - added possibility to choose ft manually for files that are not recognized (through parameter)
' 0.6.5:	 - added possibility to set custom file type for files that are not recognized (through parameter)
' 0.8:		 - added GUI for user input at the beginning
'			 - added logging mechanism
' 0.8.1:	 - secured user input at beginning (TRIM)
' 0.9:		 - added fix for 'Dependency file not found' error
' 0.9.5:	 - enhanced 'Dependency file not found' fix - now it also looks for src folder in Studio folder structure (if exists)
' 0.9.6:	 - enabled 'preserveWhiteSpace' on XML parser so that output SDLXLIFF is not 'pretty' (caused additional tab characters in target files)
' 0.9.7:	 - added support for 'Cancel' button in input window at the beginning - user can now abort processing
'

Dim sdlxliff, fso, shlObj, loger, loger_results, currFolder, fileType, fileName, extension, customFT, origFileName, regex, refFile, srcLang, srcFile, cancel

Set sdlxliff = CreateObject("MSXML.DOMDocument")
sdlxliff.preserveWhiteSpace = True

Set fso = CreateObject("Scripting.FileSystemObject")
Set shlObj = CreateObject("wscript.shell")

cancel = false

' if there are any arguments then merge them into one (because some users may not know that space separates arguments)
If WScript.Arguments.Length >= 1 Then
	For i = 0 To WScript.Arguments.Length - 1
		customFT = customFT & WScript.Arguments(i) & " "
	Next

' if there's no argument then ask user for input
Else
	customFT = InputBox("The script will try to automatically determine the file type to be set. Type in the extension or custom file type that should be used for unrecognized files, or leave empty if such files should be ignored." & vbNewLine & vbNewLine & "Examples: " & vbNewLine & vbTab & "xlsx" & vbNewLine & vbTab & "idml" & vbNewLine & vbTab & "Java Resources v 2.0.0.0" & vbNewLine, "FTFIX", " ")
	
	' if user clicks 'Cancel', an empty string is returned - treat such string as 'Cancel'
	If customFT = "" Then
		cancel = true
	Else
		customFT = Trim(customFT)
	End If
	
End If

If cancel = false Then

	' set working location to folder where script was executed
	currFolder = fso.GetAbsolutePathName(".") & "\"

	' prepare logging outputs
	Set loger = fso.CreateTextFile(currFolder &	 "FTFIX_log.log")
	Set loger_results = fso.CreateTextFile(currFolder &	 "FTFIX_results.log")

	' loop through SDLXLIFF files in current folder
	For Each currFile in fso.GetFolder(currFolder).Files
		If fso.GetExtensionName(currFile.Name) = "sdlxliff" Then
			
			loger.WriteLine " "
			loger.WriteLine currFile.Name
			loger_results.WriteLine " "
			
			' prepare DOM parser
			sdlxliff.setProperty "SelectionLanguage", "XPath"
			sdlxliff.Load(currFile.Path)
			
			fileType = sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").text
			
			If fileType = "" Then
				
				' if file type ID element is empty then go and try to fix it!
				loger.WriteLine "Empty file type definition found!"
				
				extension = guessFT(sdlxliff)
				
				If getFT(extension) <> "" Then
				
					' if file type is recognized by 'getFT' method
					loger.WriteLine "Recognized file type: " & extension
					loger.WriteLine "Setting file type to: " & getFT(extension)
					loger_results.Write currFile.Path & vbTab & "file type set to " & getFT(extension)
					
					sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").Text = getFT(extension)
					fso.CopyFile currFile.Path, currFile.Path & ".backup"
					sdlxliff.save(currFile.Path)
				Else
					If getFT(customFT) = "" Then
						If customFT = "" Then
							' file type could not be determined, leave empty
							loger.WriteLine "File type could not be determined, leaving empty."
							loger_results.Write currFile.Path & vbTab & "!! not recognized - left empty !!"
						Else
							' set *custom* file type to customFT
							loger.WriteLine "Setting *custom* file type to: " & customFT
							loger_results.Write currFile.Path & vbTab & "file type set to " & customFT & " (custom)"
							
							sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").Text = customFT
							fso.CopyFile currFile.Path, currFile.Path & ".backup"
							sdlxliff.save(currFile.Path)
						End If
					Else
						' set *custom* file type to: getFT(customFT)
						loger.WriteLine "Setting *custom* file type to: " & getFT(customFT)
						loger_results.Write currFile.Path & vbTab & "file type set to " & getFT(customFT) & " (custom)"
						
						sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").Text = getFT(customFT)
						fso.CopyFile currFile.Path, currFile.Path & ".backup"
						sdlxliff.save(currFile.Path)
					End If
				End If
				
				
			Else
				' file type definition is fine, do exactly NOTHING
				loger.WriteLine "File type definition is fine"
				loger_results.Write currFile.Path & vbTab & "filetype OK, no changes needed"
				
	' === fix for >>Reference file could not be found<< error ===
				
				srcLang = ""
				
				On Error Resume Next
				
				refFile = sdlxliff.SelectSingleNode("//*[local-name() = 'external-file']").GetAttribute("href")
				srcLang = sdlxliff.SelectSingleNode("//*[local-name() = 'file']").GetAttribute("source-language")
				
				' check if reference file path points at Temp, which would mean it's corrupted
				If InStr(refFile, "AppData\Local\Temp") <> 0 Then
				
					Set regex = New RegExp
					regex.Pattern = "^TASK[0-9]+_.*?_(.*?)$"
					
					origFileName = fso.GetFileName(sdlxliff.SelectSingleNode("//*[local-name() = 'ref-file']").GetAttribute("o-path"))
					
					' clean the 'TASK...' from the original file name, if exists
					origFileName = regex.Execute(origFileName)(0).Submatches(0)
					
					If Err <> 0 Then
						Err.Clear
					End If
					
					' that's the place where src file should be placed in orig Studio structure
					srcFile = fso.GetParentFolderName(currFolder) & "\" & srcLang & "\" & currFile.Name
					
					' so if it really exists, then use it
					If fso.FileExists(srcFile) Then
						srcLang = "..\" & srcLang & "\"
						loger.WriteLine "original source file exists: " & srcFile
					Else
						srcLang = ""
					End If
					
					origFileName = srcLang & origFileName
					
					' finally try to fix the reference file
					sdlxliff.SelectSingleNode("//*[local-name() = 'external-file']").setAttribute "href", origFileName
					
					' save the file only if everything went just fine
					If Err = 0 Then
						fso.CopyFile currFile.Path, currFile.Path & ".backup"
						sdlxliff.save(currFile.Path)
						loger.WriteLine "setting reference file to '" & origFileName & "'"
						loger_results.Write vbTab & "(reference file set to '" & origFileName & "')"
					Else
						Err.Clear
						
					End If
				End If
				
				On Error GoTo 0
				
	' === fix for >>Reference file could not be found<< error ===
				
			End If
		End If

	Next

	Set sdlxliff = Nothing

	On Error Resume Next
	shlObj.Run currFolder & "FTFIX_results.log"
	MsgBox "Done!"

End If
	
 ' FINISH '

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 '======= FUNKCJE =======
 
 ' for getting file type that matches the extension
 
 Function getFT(ByVal ext)
	
	ext = LCase(ext)
	
	Select Case ext
		Case "xlsx" getFT = "Excel 2007 v 2.0.0.0"
		Case "xls" getFT = "Excel 2003 v 2.0.0.0"
		Case "docx" getFT = "Word 2007 v 2.0.0.0"
		Case "doc" getFT = "Word 2000-2003 v 1.0.0.0"
		Case "pptx" getFT = "PowerPoint 2007 v 2.0.0.0"
		Case "ppt" getFT = "PowerPoint XP-2003 v 1.0.0.0"
		Case "inx" getFT = "Inx 1.0.0.0"
		Case "idml" getFT = "IDML v 1.0.0.0"
		Case "icml" getFT = "ICML Filter 1.0.0.0"
		Case "rtf" getFT = "RTF v 1.0.0.0"
		Case "mif" getFT = "FrameMaker 8.0 v 2.0.0.0"
		Case "odt" getFT = "Odt 1.0.0.0"
		Case "ott" getFT = "Odt 1.0.0.0"
		Case "odm" getFT = "Odt 1.0.0.0"
		Case "odp" getFT = "Odp 1.0.0.0"
		Case "otp" getFT = "Odp 1.0.0.0"
		Case "ods" getFT = "Ods 1.0.0.0"
		Case "ots" getFT = "Ods 1.0.0.0"
		Case "xtg" getFT = "QuarkXPress v 2.0.0.0"
		Case "tag" getFT = "QuarkXPress v 2.0.0.0"
		Case "xlf" getFT = "XLIFF 1.1-1.2 v 2.0.0.0"
		Case "xliff" getFT = "XLIFF 1.1-1.2 v 2.0.0.0"
		Case "xlz" getFT = "XLIFF 1.1-1.2 v 2.0.0.0"
		Case "mqxlf" getFT = "MemoQ v 1.0.0.0"
		Case "mqxliff" getFT = "MemoQ v 1.0.0.0"
		Case "mqxlz" getFT = "MemoQ v 1.0.0.0"
		Case "pdf" getFT = "PDF v 2.0.0.0"
		Case "csv" getFT = "CSV v 2.0.0.0"
		Case "properties" getFT = "Java Resources v 2.0.0.0"
		Case "po" getFT = "PO files v 1.0.0.0"
		Case "srt" getFT = "SubRip v 1.0.0.0"
		Case "resx" getFT = "XML: RESX v 1.2.0.0"
		Case "its" getFT = "XML: ITS 1.0 v 1.2.0.0"
		Case "txt" getFT = "Plain Text v 1.0.0.0"
		Case "ttx" getFT = "TTX 2.0 v 2.0.0.0"
		Case "itd" getFT = "ITD v 1.0.0.0"
		
		
		Case Else getFT = ""
	End Select
 
 End Function
 
 
 ' for recognizing the file type based on file's structure
 
 Function guessFT(ByRef xmlFile)
	On Error Resume Next
	
	guessFT = fso.GetExtensionName(sdlxliff.SelectSingleNode("//*[local-name() = 'file']").GetAttribute("original"))
	'WScript.Echo "guessFT file: " & guessFT
	loger.WriteLine "guessFT 'file': " & guessFT
	If guessFT = "" Or guessFT = "tmp" Then
		guessFT = fso.GetExtensionName(sdlxliff.SelectSingleNode("//*[local-name() = 'ref-file']").GetAttribute("name"))
		'WScript.Echo "guessFT ref-file@name: " & guessFT
		loger.WriteLine "guessFT 'ref-file@name': " & guessFT
		If guessFT = "" Or guessFT = "tmp" Then
			guessFT = fso.GetExtensionName(sdlxliff.SelectSingleNode("//*[local-name() = 'ref-file']").GetAttribute("o-path"))
			'WScript.Echo "guessFT ref-file@o-path: " & guessFT
			loger.WriteLine "guessFT 'ref-file[@o-path]': " & guessFT
			If guessFT = "" Or guessFT = "tmp" Then
				guessFT = fso.GetExtensionName(sdlxliff.SelectSingleNode("//*[local-name() = 'value'][@key='SDL:OriginalFilePath']").text)
				'WScript.Echo "guessFT value[@key='SDL:OriginalFilePath']: " & guessFT
				loger.WriteLine "guessFT 'value[@key='SDL:OriginalFilePath']': " & guessFT
				If guessFT = "" Or guessFT = "tmp" Then
					' bad luck, file type ID wasn't recognized :/
				End If
			End If
		End If
	End If
 On Error GoTo 0
 
 End Function