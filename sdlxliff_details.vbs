' by Marcin Murawski || created: 2016-04-08 || last update: 2016-05-05
'
'
' 0.5:	 - initial
' 1.0:	 - output log file is saved as XLXML
'

Dim sdlxliff, fso, shlObj, logFileName, loger, currFolder, fileType, fileName, srcLang, tgtLang, content(2048, 4), rowNum

Set sdlxliff = CreateObject("MSXML.DOMDocument")
sdlxliff.preserveWhiteSpace = True

Set fso = CreateObject("Scripting.FileSystemObject")
Set shlObj = CreateObject("wscript.shell")

If WScript.Arguments.Length >= 1 Then
	' TO DO: subfolders option

End If

' set working location to folder where script was executed
currFolder = fso.GetAbsolutePathName(".") & "\"

' prepare logging outputs
logFileName = "sdlxliff_details.xls"
'Set loger = fso.CreateTextFile(currFolder & logFileName)

rowNum = 1

content(0, 0) = "File name"
content(0, 1) = "File type"
content(0, 2) = "Source language"
content(0, 3) = "Target language"

' loop through SDLXLIFF files in current folder
For Each currFile in fso.GetFolder(currFolder).Files
	If fso.GetExtensionName(currFile.Name) = "sdlxliff" Then
		
		' prepare DOM parser
		sdlxliff.setProperty "SelectionLanguage", "XPath"
		sdlxliff.Load(currFile.Path)
		
		fileName = currFile.Name
		fileType = sdlxliff.SelectSingleNode("//*[local-name() = 'filetype-id']").text
		srcLang = sdlxliff.SelectSingleNode("//*[local-name() = 'file']").GetAttribute("source-language")
		tgtLang = sdlxliff.SelectSingleNode("//*[local-name() = 'file']").GetAttribute("target-language")
		
		
		content(rowNum, 0) = fileName
		content(rowNum, 1) = fileType
		content(rowNum, 2) = srcLang
		content(rowNum, 3) = tgtLang
		
		rowNum = rowNum + 1
		
	End If

Next

saveAsXLXML content, rowNum, logFileName

Set sdlxliff = Nothing

	On Error Resume Next
shlObj.Run currFolder & logFileName
MsgBox "Done!"

	
 ' FINISH '
 
 
 
Function saveAsXLXML(ByVal tab, ByVal rowCount, ByVal fileName)
	Dim header, footer, xlxml, currFol, objFso
	
	header = "<?xml version=""1.0""?><?mso-application progid=""Excel.Sheet""?><Workbook xmlns=""urn:schemas-microsoft-com:office:spreadsheet"" xmlns:o=""urn:schemas-microsoft-com:office:office"" xmlns:x=""urn:schemas-microsoft-com:office:excel"" xmlns:ss=""urn:schemas-microsoft-com:office:spreadsheet"" xmlns:html=""http://www.w3.org/TR/REC-html40""><DocumentProperties xmlns=""urn:schemas-microsoft-com:office:office"">  <Author>Marcin Murawski</Author>  <LastAuthor>Marcin Murawski</LastAuthor>  <Created>2016-05-16T08:27:31Z</Created>  <Version>14.00</Version> </DocumentProperties> <OfficeDocumentSettings xmlns=""urn:schemas-microsoft-com:office:office"">  <AllowPNG/> </OfficeDocumentSettings> <ExcelWorkbook xmlns=""urn:schemas-microsoft-com:office:excel"">  <WindowHeight>13350</WindowHeight> <WindowWidth>27795</WindowWidth>  <WindowTopX>480</WindowTopX>  <WindowTopY>30</WindowTopY>  <RefModeR1C1/>  <ProtectStructure>False</ProtectStructure>  <ProtectWindows>False</ProtectWindows> </ExcelWorkbook> <Styles>  <Style ss:ID=""Default"" ss:Name=""Normal"">   <Alignment ss:Vertical=""Bottom""/>   <Borders/>   <Font ss:FontName=""Calibri"" x:Family=""Swiss"" ss:Size=""11"" ss:Color=""#000000""/>   <Interior/>   <NumberFormat/>  <Protection/>  </Style> </Styles><Worksheet ss:Name=""" & fileName & """><Table ss:ExpandedColumnCount=""5000"" ss:ExpandedRowCount=""" & rowCount & """ x:FullColumns=""1"" x:FullRows=""1"" ss:DefaultRowHeight=""15""><Column ss:Width=""400""/><Column ss:Width=""150""/><Column ss:Width=""100""/><Column ss:Width=""100""/>"
	footer = "</Table><WorksheetOptions xmlns=""urn:schemas-microsoft-com:office:excel""><PageSetup><Header x:Margin=""0.3""/><Footer x:Margin=""0.3""/><PageMargins x:Bottom=""0.75"" x:Left=""0.7"" x:Right=""0.7"" x:Top=""0.75""/></PageSetup><Selected/><ProtectObjects>False</ProtectObjects><ProtectScenarios>False</ProtectScenarios></WorksheetOptions></Worksheet></Workbook>"
	
	Set objFso = CreateObject("Scripting.FileSystemObject")
	currFol = objFso.GetAbsolutePathName(".") & "\"
	Set xlxml = objFso.CreateTextFile(currFol &	fileName)
	
	xlxml.Write header
	
	For i = 0 to UBound(tab, 1)
		If (tab(i, 0) <> "") Then
			xlxml.Write "<Row>"
		End If
		
		For j = 0 to UBound(tab, 2)
			If (tab(i, j) <> "") Then
				xlxml.Write "<Cell><Data ss:Type=""String"">"
				'WScript.Echo i & ", " & j & vbTab & tab(i, j)
				xlxml.Write tab(i, j)
				xlxml.Write "</Data></Cell>"
			End If
		Next
		
		If (tab(i, 0) <> "") Then
			xlxml.Write "</Row>"
		End If
	Next
	
	xlxml.Write footer
	xlxml.Close
	
End Function


















