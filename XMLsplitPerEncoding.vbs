' by Marcin Murawski || created: 2016-06-21 || last update: 2016-06-21
'
' Script copies XML files into folders named like their encodings.
'
' HOW:
' Run the script in folder with XML files. Encoding folders will be created and XML files will be copied into them.
'

Dim fso, reg, currFolder, currentFile, fullPath, xml, header, encoding, encodingFolder

Set reg = New RegExp
reg.Pattern = "encoding=""(.*?)"""

Set fso = CreateObject("Scripting.FileSystemObject")

currFolder = fso.GetAbsolutePathName(".") & "\"

For Each currentFile in fso.GetFolder(currFolder).Files
	If fso.GetExtensionName(currentFile.Name) = "xml" Then
		WScript.Echo currentFile.Name
		
		fullPath = currFolder & currentFile.Name
		
		Set xml = fso.OpenTextFile(fullPath)
		header = xml.ReadLine
		
		encoding = reg.Execute(header)(0).SubMatches(0)
	
		WScript.Echo vbTab & encoding
	
		endodingFolder = currFolder & encoding
	
		If Not fso.FolderExists(endodingFolder) Then
			fso.CreateFolder(endodingFolder)
		End If
	
		fso.CopyFile currentFile.Path, endodingFolder & "\" & currentFile.Name
	
	
	End If

Next

MsgBox "Done!"